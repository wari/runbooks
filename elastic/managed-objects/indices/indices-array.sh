#!/bin/bash

declare -a indices

export indices=(
  camoproxy
  chef
  consul
  gitaly
  gke
  jaeger
  mailroom
  monitoring
  pages
  postgres
  praefect
  pubsubbeat
  puma
  rails
  redis
  registry
  runner
  shell
  sidekiq
  system
  vault
  workhorse
)
